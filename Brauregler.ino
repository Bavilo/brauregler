#define version_number          "    Version 1.5.0   "

//***********************************************************
// Bibliotheken               // Libraries
//***********************************************************
#include <OneWire.h>                    // 1-Wire Bus Verbindung                            // 1-Wire bus connection
#include <DallasTemperature.h>          // Digitale Temperaturfühler Verbindung (DS18B20)   // Digital temperature connection (DS18B20)
#include <CountUpDownTimer.h>           // Auf- und Abwärtszähler                           // Up- and downcounter
#include <EEPROM.h>                     // Interner Speicher                                // Internal storage
#include <LiquidCrystal_I2C.h>          // I2C LC-Display                                   // I2C LC-Display

//***********************************************************
// Benutzereinstellungen     // User settings
//***********************************************************
DeviceAddress sensor_hg_one = { 0x28, 0xFF, 0x41, 0xE9, 0x70, 0x14, 0x04, 0xEF };           // Adresse für Hauptgusssensor 1  (Bitte mit richtigen Werten auffüllen!)
DeviceAddress sensor_hg_two = { 0x28, 0xFF, 0xF4, 0xEA, 0x70, 0x14, 0x04, 0xFA };           // Adresse für Hauptgusssensor 2  (Bitte mit richtigen Werten auffüllen!)
DeviceAddress sensor_ng_one = { 0x28, 0xFF, 0x4B, 0xC7, 0x70, 0x14, 0x04, 0x37 };           // Adresse für Nachgusssensor 1   (Bitte mit richtigen Werten auffüllen!)
DeviceAddress sensor_ng_two = { 0x28, 0xFF, 0xB0, 0x04, 0x71, 0x14, 0x04, 0xFB };           // Adresse für Nachgusssensor 2   (Bitte mit richtigen Werten auffüllen!)

#define target_ng_temperature       78      // Temperatur für die Nachgussteuerung              // Temperature for sparging control
#define target_cooking_temperature  97      // Kochtemperatur (in °C)                           // Cooking temperature (in °C)

#define pulses_per_liter          82.0      // Anzahl an Impulsen pro Liter, spezifischer Wert des Wasserzählers. Dieser muss ermittelt werden. MUSS in Dezimal angeben werden!!!
                                            // Amount of impulses per Liter, specific value of the watercounter. This value must be determined. MUST be a decimal value!!!

#define water_adjustment          0.15      // Menge an Wasser (in Liter) im Pumpenkreislauf - Sie wird bei "target_sparge_amount" draufgerechnet
                                            // Amount of water (in Liter) which remains in the pumpcirculation - It will be added to "target_sparge_amount"

#define level_switch_lg_in_direction   LOW      // Eingang - Schaltlogik für den Schwimmerschalter des Läutergrants                                                           
                                                // LOW = Schwimmerschalter zeigt nach oben
                                                // HIGH = Schwimmerschalter zeigt nach unten
                                                // Die beiden Werte umkehren falls benötigt

#define level_switch_lb_in_direction  HIGH      // Eingang - Schaltlogik für den Schwimmerschalter des Läuterbottichs    
                                                // LOW = Schwimmerschalter zeigt nach oben
                                                // HIGH = Schwimmerschalter zeigt nach unten
                                                // Die beiden Werte umkehren falls benötigt                  

#define heater_hg_out_ON              HIGH      // Ausgang - Schaltlogik für das Hauptgussrelais                                          // Output - Switching logic for the mashing water relay
#define heater_hg_out_OFF              LOW      // Die beiden Werte umkehren falls benötigt                                               // Reverse the two values if needed

#define heater_ng_out_ON              HIGH      // Ausgang - Schaltlogik für das Nachgussrelais                                           // Output - Switching logic for the sparging water relay
#define heater_ng_out_OFF              LOW      // Die beiden Werte umkehren falls benötigt                                               // Reverse the two values if needed

#define pump_lg_out_ON                HIGH      // Ausgang - Schaltlogik für das Läutergrantrelais                                        // Output - Switching logic for the wortpump relay
#define pump_lg_out_OFF                LOW      // Die beiden Werte umkehren falls benötigt                                               // Reverse the two values if needed

#define pump_lb_out_ON                HIGH      // Ausgang - Schaltlogik für das Läuterbottichrelais                                      // Output - Switching logic for the lautertun relay
#define pump_lb_out_OFF                LOW      // Die beiden Werte umkehren falls benötigt                                               // Reverse the two values if needed

#define stirrer_out_ON                HIGH      // Ausgang - Schaltlogik für das Rührwerkrelais                                           // Output - Switching logic for the stirrer relay
#define stirrer_out_OFF                LOW      // Die beiden Werte umkehren falls benötigt                                               // Reverse the two values if needed  

#define lcd_enable                    true      // Aktiviert das LC-Display                                                               // Activates the LC-Display
#define i2c_addr                      0x27      // Adresse für das I2C LC-Display                                                         // Address for the I2C LC-Display

#define debug_mode                    true      // Wenn aktiviert dann wird der momentane Kontroller Status im Serial Monitor             // When activated, the momentary status of the controller will be shown in the serial monitor                                      

//***********************************************************
// Konstante                // Constants
//***********************************************************
#define flow_sensor_in           2      // Eingang - Wasserzähler                           // Input - Watercounter
#define heater_hg_automatic_in  26      // Eingang - Heizung (Hauptguss) Automatik          // Input - Heater (Mashing water) automatic
#define heater_hg_manual_in     27      // Eingang - Heizung (Hauptguss) Manuell            // Input - Heater (Mashing water) manual
#define heater_ng_automatic_in  28      // Eingang - Heizung (Nachguss) Automatik           // Input - Heater (Sparging water) automatic
#define heater_ng_manual_in     29      // Eingang - Heizung (Nachguss) Manuell             // Input - Heater (Sparging water) manual
#define pump_lg_automatic_in    30      // Eingang - Pumpe (Läutergrant) Automatik          // Input - Pump () automatic
#define pump_lg_manual_in       31      // Eingang - Pumpe (Läutergrant) Manuell            // Input - Pump () manual
#define pump_lb_automatic_in    32      // Eingang - Pumpe (Läuterbottich) Automatik        // Input - Pump (Lautertun) automatic
#define pump_lb_manual_in       33      // Eingang - Pumpe (Läuterbottich) Manuell          // Input - Pump (Lautertun) manual
#define level_switch_lg_in      34      // Eingang - Niveauschalter (Läutergrant)           // Input - Levelswitch ()
#define level_switch_lb_in      35      // Eingang - Niveauschalter (Läuterbottich)         // Input - Levelswitch (Lautertun)
#define one_wire_bus            36      // Eingang - Temperatursensoren                     // Input - Temperature sensor
#define stirrer_automatic_in    37      // Eingang - Rührwerk Automatik                     // Input - Stirrer automatic
#define stirrer_manual_in       38      // Eingang - Rührwerk Manuell                       // Input - Stirrer manual

#define speaker_out             12      // Ausgang - Piezo buzzer                           // Output - Piezo buzzer
#define lcd_sda                 20      // Ausgang - LCD SDA                                // Output - LCD SDA
#define lcd_scl                 21      // Ausgang - LCD SCL                                // Output - LCD SCL
#define heater_hg_out           41      // Ausgang - Heizen (Hauptguss)                     // Output - Heater (Mashing water)
#define heater_ng_out           42      // Ausgang - Heizen (Nachguss)                      // Output - Heater (Sparging water)
#define pump_lg_out             43      // Ausgang - Pumpe (Läutergrant)                    // Output - Pump ()
#define pump_lb_out             44      // Ausgang - Pumpe (Läuterbottich)                  // output - Pump (Lautertun)
#define stirrer_out             45      // Ausgang - Rührwerk                               // Output - Stirrer

#define max_rests                7      // Maximale Anzahl an Rasten                        // Max amount of rests
#define max_hops                 7      // Maximale Anzahl an Hopfengaben                   // Max amount of hoppings

#define baud_rate_wifi      115200      // Die Baudrate für das Wifi-Modul                                                        // Baudrate for the wifi modul
#define baud_rate_serial     57600      // Die Baudrate für das Bluetooth-Modul                                                   // Baudrate for the bluetooth modul
#define baud_rate_bluetooth  57600      // Die Baudrate für den Serial Monitor                                                    // Baudrate for the serial monitor
#define serial_time            500      // Intervall (in Millisekunden) für das Senden von seriellen Daten                        // Interval (in milliseconds) for the sending of serial data
#define sensor_time           1000      // Intervall (in Millisekunden) für das Verarbeiten der Temperatursensoren                // Interval (in milliseconds) for processing temperature sensor data
#define lcd_time              5000      // Intervall (in Millisekunden) in der das LCD zwischen Nachgüssen und Kochen umschaltet  // Interval (in milliseconds) for the LCD to switch between sparging and cooking if they run parallel
#define debugging_time        5000      // Intervall (in Millisekunden) in der Debugging-Informationen im Serial Monitor gezeigt werden // Interval (in milliseconds) to show debugging information in the serial monitor

#define note_duration          500      // Die Dauer für die eine Note gespielt wird                                              // Duration for which the note will be played
#define note_pause            1000      // Die Dauer der Pause zwischen zwei Noten                                                // Duration for the pause between two notes

//***********************************************************
// Variablen          // Variables
//***********************************************************
volatile int pulses = 0;

String raw_serial_data;
String processed_serial_data;
String send_data;

String hop_notification = "false";
String vwh_notification = "false";
String is_vwh = "false";

char delimiter = ',';
char termination = '!';

unsigned long current_time;
unsigned long temp_serial_time;
unsigned long temp_sensor_time;
unsigned long temp_lcd_time;
unsigned long temp_debugging_time;
unsigned long temp_speaker_time;

signed long heater_hg_switching_time = -5000;
signed long heater_ng_switching_time = -5000;

float hysteresis_lg = 0.5;
float hysteresis_lb = 0.5;

int pump_lg_duration = 10;
int pump_lb_duration = 10;

float current_sparge_amount;
float target_sparge_amount;
float remaining_sparge_amount;

float current_hg_temperature;
float current_ng_temperature;

int rest_temperatures[max_rests];
int rest_times[max_rests];
int rest_times_in_hours[max_rests];
int rest_times_in_minutes[max_rests];

int target_mashing_temperature;

int hop_times[max_hops];
int hop_times_in_hours[max_hops];
int hop_times_in_minutes[max_hops];

int target_cooking_time;
int target_cooking_time_in_hours;
int target_cooking_time_in_minutes;

int rests_amount;
int hops_amount;

int rest_index;
int hop_index;

int current_time_in_hours;
int current_time_in_minutes;
int current_time_in_seconds;

int current_hop_time_in_hours;
int current_hop_time_in_minutes;
int current_hop_time_in_seconds;

int controller_state;     /* 0 = Warte auf Rezept                                 0 = Waiting for recipe
                             1 = Einmaischen gestartet                            1 = Mashing in started
                             2 = Einmaischen beendet                              2 = Mashing in finished
                             3 = Maischen gestartet                               3 = Mashing started
                             4 = Maischen beendet                                 4 = Mashing finished
                             5 = Nachgüsse gestartet                              5 = Sparging started
                             6 = Nachgüsse beendet                                6 = Sparging finished
                             7 = Nachgüsse gestartet UND Kochen gestartet         7 = Sparging started AND cooking started
                             8 = Nachgüsse beendet UND Kochen gestaret            8 = Sparging finished AND cooking started
                             9 = Kochen beendet                                   9 = Cooking finished */

boolean timer_mashing_started = false;
boolean timer_cooking_started = false;
boolean timer_hop_started = false;

boolean sensor_hg_one_ok = false;
boolean sensor_hg_two_ok = false;
boolean sensor_ng_one_ok = false;
boolean sensor_ng_two_ok = false;

boolean heater_hg_state_automatic = false;
boolean heater_hg_state_manual = false;
boolean heater_ng_state_automatic = false;
boolean heater_ng_state_manual = false;
boolean pump_lg_state_automatic = false;
boolean pump_lg_state_manual = false;
boolean pump_lb_state_automatic = false;
boolean pump_lb_state_manual = false;
boolean level_switch_lg_full = false;
boolean level_switch_lb_full = false;
boolean stirrer_state_automatic = false;
boolean stirrer_state_manual = false;

boolean ng_enable = true;
boolean ng_temperature_reached = false;

boolean vwh_notification_sent = false;

boolean lcd_swap = false;

boolean bluetooth = false;

int melody[] = { 300, 300, 300, 300, 300 };
bool melody_play_state[5];

int melody_index;
int melody_play_state_index;

//***********************************************************
// Objekte          // Objects
//***********************************************************
OneWire one_wire(one_wire_bus);

DallasTemperature sensors(&one_wire);

CountUpDownTimer mashing_timer(DOWN);
CountUpDownTimer cooking_timer(DOWN);
CountUpDownTimer hop_timer(DOWN);
CountUpDownTimer pump_lg_timer(DOWN);
CountUpDownTimer pump_lb_timer(DOWN);

LiquidCrystal_I2C lcd(i2c_addr, 20, 4);

//***********************************************************
// Setup
//***********************************************************
void setup(void) {
  Serial.begin(baud_rate_serial);
  Serial1.begin(baud_rate_bluetooth);
  Serial2.begin(baud_rate_wifi);

  sensors.begin();
  sensors.setResolution(sensor_hg_one, 12);
  sensors.setResolution(sensor_hg_two, 12);
  sensors.setResolution(sensor_ng_one, 12);
  sensors.setResolution(sensor_ng_two, 12);
  sensors.setWaitForConversion(false);

  pinMode(flow_sensor_in, INPUT);

  pinMode(heater_hg_automatic_in, INPUT_PULLUP);
  pinMode(heater_hg_manual_in, INPUT_PULLUP);
  pinMode(heater_ng_automatic_in, INPUT_PULLUP);
  pinMode(heater_ng_manual_in, INPUT_PULLUP);
  pinMode(pump_lg_automatic_in, INPUT_PULLUP);
  pinMode(pump_lg_manual_in, INPUT_PULLUP);
  pinMode(pump_lb_automatic_in, INPUT_PULLUP);
  pinMode(pump_lb_manual_in, INPUT_PULLUP);
  pinMode(level_switch_lg_in, INPUT_PULLUP);
  pinMode(level_switch_lb_in, INPUT_PULLUP);
  pinMode(stirrer_automatic_in, INPUT_PULLUP);
  pinMode(stirrer_manual_in, INPUT_PULLUP);

  pinMode(heater_hg_out, OUTPUT);
  pinMode(heater_ng_out, OUTPUT);
  pinMode(pump_lg_out, OUTPUT);
  pinMode(pump_lb_out, OUTPUT);
  pinMode(stirrer_out, OUTPUT);

  digitalWrite(heater_hg_out, heater_hg_out_OFF);
  digitalWrite(heater_ng_out, heater_ng_out_OFF);
  digitalWrite(pump_lg_out, pump_lg_out_OFF);
  digitalWrite(pump_lb_out, pump_lb_out_OFF);
  digitalWrite(stirrer_out, stirrer_out_OFF);

  attachInterrupt(digitalPinToInterrupt(flow_sensor_in), increasePulses, RISING);

  if (EEPROM.read(0) != 0xFF) {
    hysteresis_lg = EEPROM.read(0) * 0.1;
  }

  if (EEPROM.read(1) != 0xFF) {
    hysteresis_lb = EEPROM.read(1) * 0.1;
  }

  if (EEPROM.read(2) != 0xFF) {
    pump_lg_duration = EEPROM.read(2);
  }

  if (EEPROM.read(3) != 0xFF) {
    pump_lb_duration = EEPROM.read(3);
  }

  if (lcd_enable) {
    lcd.begin();    
    lcd.backlight();
  }  
}

//***********************************************************
// Loop
//***********************************************************
void loop(void) { 
  mashing_timer.Timer();
  cooking_timer.Timer();
  hop_timer.Timer();
  pump_lg_timer.Timer();
  pump_lb_timer.Timer();

  current_time = millis();

  current_sparge_amount = pulses / pulses_per_liter;

  if (debug_mode) {
    printDebugging();
  }
    
  //*****************************************************************************
  // Modus - Piezo Buzzer                     // Mode - Piezo Buzzer
  //*****************************************************************************
  switch(controller_state) {
    case 2:
      if (!melody_play_state[0]) {
        beep();        
      }
      break;
    case 4:
      if (!melody_play_state[1]) {
        beep();        
      }
      break;
    case 6:
      if (!melody_play_state[2]) {
        beep();
      }
      break;
    case 8:
      if (!melody_play_state[3]) {
        beep();
      }
      break;
    case 9:
      if (!melody_play_state[4]) {
        beep();
      }      
      break;
  }  

  //*****************************************************************************
  // Modus - Heizung (Hauptguss)            // Mode - Heating (Mashing water)
  //*****************************************************************************
  if (digitalRead(heater_hg_manual_in) == LOW) {
    heater_hg_state_manual = true;
    heater_hg_state_automatic = false;
  } else if (digitalRead(heater_hg_automatic_in) == LOW) {
    heater_hg_state_manual = false;
    heater_hg_state_automatic = true;
  } else {
    heater_hg_state_manual = false;
    heater_hg_state_automatic = false;
  }

  //*****************************************************************************
  // Modus - Heizung (Nachguss)             // Mode - Heating (Sparging water)
  //*****************************************************************************
  if (digitalRead(heater_ng_manual_in) == LOW) {
    heater_ng_state_manual = true;
    heater_ng_state_automatic = false;
  } else if (digitalRead(heater_ng_automatic_in) == LOW) {
    heater_ng_state_manual = false;
    heater_ng_state_automatic = true;
  } else {
    heater_ng_state_manual = false;
    heater_ng_state_automatic = false;
  }

  //*****************************************************************************
  // Modus - Pumpe (Läutergrant)            // Mode - Pump (wortpump)
  //*****************************************************************************
  if (digitalRead(pump_lg_manual_in) == LOW) {
    pump_lg_state_manual = true;
    pump_lg_state_automatic = false;
  } else if (digitalRead(pump_lg_automatic_in) == LOW) {
    pump_lg_state_manual = false;
    pump_lg_state_automatic = true;
  } else {
    pump_lg_state_manual = false;
    pump_lg_state_automatic = false;
  }

  //*****************************************************************************
  // Modus - Pumpe (Läuterbottich)          // Mode - Pump (Lautertun)
  //*****************************************************************************
  if (digitalRead(pump_lb_manual_in) == LOW) {
    pump_lb_state_manual = true;
    pump_lb_state_automatic = false;
  } else if (digitalRead(pump_lb_automatic_in) == LOW) {
    pump_lb_state_manual = false;
    pump_lb_state_automatic = true;
  } else {
    pump_lb_state_manual = false;
    pump_lb_state_automatic = false;
  }

  //*****************************************************************************
  // Modus - Niveauschalter (Läutergrant)   // Mode - Levelswitch (wortpump)
  //*****************************************************************************
  if (digitalRead(level_switch_lg_in) == level_switch_lg_in_direction) {
    level_switch_lg_full = true;
  } else {
    level_switch_lg_full = false;
  }

  //*****************************************************************************
  // Modus - Niveauschalter (Läuterbottich) // Mode - Levelswitch (Lautertun)
  //*****************************************************************************
  if (digitalRead(level_switch_lb_in) == level_switch_lb_in_direction) {
    level_switch_lb_full = true;
  } else {
    level_switch_lb_full = false;
  }

  //*****************************************************************************
  // Modus - Rührwerk                       // Mode - Stirrer
  //*****************************************************************************
  if (digitalRead(stirrer_manual_in) == LOW) {
    stirrer_state_manual = true;
    stirrer_state_automatic = false;
  } else if (digitalRead(stirrer_automatic_in) == LOW) {
    stirrer_state_manual = false;
    stirrer_state_automatic = true;
  } else {
    stirrer_state_manual = false;
    stirrer_state_automatic = false;
  }

  //*****************************************************************************
  // Steuerung - Heizung (Nachguss)         // Controller - Heating (Sparging water)
  // Läuft unabhängig von controller_state  // Runs independant from controller_state
  //*****************************************************************************
  if (heater_ng_state_automatic) {
    if (controller_state > 0) {
      if (ng_enable) {
        if (sensor_ng_one_ok || sensor_ng_two_ok) {
          if (current_ng_temperature > target_ng_temperature - hysteresis_lb && current_ng_temperature != 85.00 && millis() >= heater_ng_switching_time + 5000UL) {
            heater_ng_switching_time = millis();
  
            ng_temperature_reached = true;
  
            digitalWrite(heater_ng_out, heater_ng_out_OFF);
          } else if (current_ng_temperature <= target_ng_temperature - hysteresis_lb && current_ng_temperature != 85.00 && millis() >= heater_ng_switching_time + 5000UL) {
            heater_ng_switching_time = millis();
  
            ng_temperature_reached = false;
  
            digitalWrite(heater_ng_out, heater_ng_out_ON);
          }
        } else {
          digitalWrite(heater_ng_out, heater_ng_out_OFF);
        }
      } else {
        digitalWrite(heater_ng_out, heater_ng_out_OFF);
      }
    }
  } else if (heater_ng_state_manual) {
    digitalWrite(heater_ng_out, heater_ng_out_ON);
  } else {
    digitalWrite(heater_ng_out, heater_ng_out_OFF);
  }

  //*****************************************************************************
  // Steuerung - Pumpe (Läutergrant)        // Controller - Pump ()
  // Läuft unabhängig von controller_state  // Runs independant from controller_state
  //*****************************************************************************
  if (pump_lg_state_automatic) {
    if (level_switch_lg_full) {
      pump_lg_timer.SetTimer(0, 0, pump_lg_duration);
      pump_lg_timer.StartTimer();

      digitalWrite(pump_lg_out, pump_lg_out_ON);
    }

    if (TimeCheck(pump_lg_timer, 0, 0, 0)) {
      pump_lg_timer.StopTimer();

      digitalWrite(pump_lg_out, pump_lg_out_OFF);
    }
  } else if (pump_lg_state_manual) {
    digitalWrite(pump_lg_out, pump_lg_out_ON);
  } else {
    digitalWrite(pump_lg_out, pump_lg_out_OFF);
  }

  //*****************************************************************************
  // Steuerung - Rührwerk                               // Controller - Stirrer
  // Nach Erhalt eines Rezept aktiv bis zum Abmaischen  // Active upon receiving a recipe and until end of mashing
  //*****************************************************************************
  if (stirrer_state_automatic) {
    if (controller_state > 0 && controller_state < 4) {
      digitalWrite(stirrer_out, stirrer_out_ON);
    }
  } else if (stirrer_state_manual) {
    digitalWrite(stirrer_out, stirrer_out_ON);
  } else {
    digitalWrite(stirrer_out, stirrer_out_OFF);
  }

  //*****************************************************************************
  // Empfange Daten von Android         // Receive data from Android
  //*****************************************************************************
  if (Serial1.available() || Serial2.available()) {
    if (Serial1.available() > 0) {
      raw_serial_data = Serial1.readStringUntil('!');
      
      bluetooth = true;
    }

    if (Serial2.available() > 0) {
      raw_serial_data = Serial2.readStringUntil('!');
      
      bluetooth = false;
    }    

    switch (raw_serial_data.charAt(0)) {
      case 'I':
        processed_serial_data = raw_serial_data.substring(1, raw_serial_data.length());

        rests_amount = splitString(processed_serial_data, ',', 0).toInt();
        target_mashing_temperature = splitString(processed_serial_data, ',', 1).toInt();

        for (int i = 0; i < rests_amount; i++) {
          rest_temperatures[i] = splitString(processed_serial_data, ',', 2 + i).toInt();
        }

        for (int i = 0; i < rests_amount; i++) {
          rest_times[i] = splitString(processed_serial_data, ',', 9 + i).toInt();
        }

        target_sparge_amount = splitString(processed_serial_data, ',', 16).toFloat();

        hops_amount = splitString(processed_serial_data, ',', 17).toInt();
        target_cooking_time = splitString(processed_serial_data, ',', 18).toInt();

        for (int i = 0; i < hops_amount; i++) {
          hop_times[i] = splitString(processed_serial_data, ',', 19 + i).toInt();
        }

        is_vwh = splitString(processed_serial_data, ',', 26);        

        for (int i = 0; i < rests_amount; i++) {
          if (rest_times[i] >= 60) {
            rest_times_in_hours[i] = rest_times[i] / 60;
            rest_times_in_minutes[i] = rest_times[i] - 60;
          } else {
            rest_times_in_hours[i] = 0;
            rest_times_in_minutes[i] = rest_times[i];
          }
        }

        if (target_cooking_time >= 60) {
          target_cooking_time_in_hours = target_cooking_time / 60;
          target_cooking_time_in_minutes = target_cooking_time - 60;
        } else {
          target_cooking_time_in_hours = 0;
          target_cooking_time_in_minutes = target_cooking_time;
        }

        for (int i = 0; i < hops_amount; i++) {
          if (hop_times[i] >= 60) {
            hop_times_in_hours[i] = hop_times[i] / 60;
            hop_times_in_minutes[i] = hop_times[i] - 60;
          } else {
            hop_times_in_hours[i] = 0;
            hop_times_in_minutes[i] = hop_times[i];
          }
        }

        controller_state = 1;
        break;
      case 'M':
        controller_state = 3;
        break;
      case 'S':
        controller_state = 5;
        break;
      case 'K':
        if (controller_state == 5) {
          controller_state = 7;
        } else {
          controller_state = 8;
        }
        break;
      case 'E':
        processed_serial_data = raw_serial_data.substring(1, raw_serial_data.length());

        hysteresis_lg = splitString(processed_serial_data, ',', 0).toFloat();
        hysteresis_lb = splitString(processed_serial_data, ',', 1).toFloat();

        pump_lg_duration = splitString(processed_serial_data, ',', 2).toInt();
        pump_lb_duration = splitString(processed_serial_data, ',', 3).toInt();

        EEPROM.write(0, hysteresis_lg * 10.0);
        EEPROM.write(1, hysteresis_lb * 10.0);
        EEPROM.write(2, pump_lg_duration);
        EEPROM.write(3, pump_lb_duration);
        break;
      case 'A':
        digitalWrite(heater_hg_out, heater_hg_out_OFF);

        controller_state = 4;
        break;
      case 'B':
        digitalWrite(pump_lb_out, pump_lb_out_OFF);

        controller_state = 6;
        break;
      case 'C':
        digitalWrite(heater_hg_out, heater_hg_out_OFF);

        controller_state = 9;
        break;
      case '-':
        if (timer_mashing_started) {
          if (mashing_timer.ShowHours() == 0 && mashing_timer.ShowMinutes() <= 4 && mashing_timer.ShowSeconds() <= 59) {
            mashing_timer.SetTimer(0, 0, 0);

            mashing_timer.StopTimer();

            timer_mashing_started = false;
          } else if (mashing_timer.ShowHours() == 1 && mashing_timer.ShowMinutes() <= 4 && mashing_timer.ShowSeconds() <= 59) {
            mashing_timer.SetTimer(0, (60 + mashing_timer.ShowMinutes()) - 5, mashing_timer.ShowSeconds());
          } else {
            mashing_timer.SetTimer(mashing_timer.ShowHours(), mashing_timer.ShowMinutes() - 5, mashing_timer.ShowSeconds());
          }

          if (rest_times[rest_index] >= 5) {
            rest_times[rest_index] -= 5;
          } else {
            rest_times[rest_index] = 0;
          }

          if (rest_times[rest_index] >= 60) {
            rest_times_in_hours[rest_index] = rest_times[rest_index] / 60;
            rest_times_in_minutes[rest_index] = rest_times[rest_index] - 60;
          } else {
            rest_times_in_hours[rest_index] = 0;
            rest_times_in_minutes[rest_index] = rest_times[rest_index];
          }
        }
        break;
      case '+':
        if (timer_mashing_started) {
          if (mashing_timer.ShowHours() == 0 && mashing_timer.ShowMinutes() >= 55 && mashing_timer.ShowSeconds() >= 0) {
            mashing_timer.SetTimer(1, 5 - (60 - mashing_timer.ShowMinutes()), mashing_timer.ShowSeconds());
          } else if (mashing_timer.ShowHours() == 1 && mashing_timer.ShowMinutes() >= 25 && mashing_timer.ShowSeconds() >= 0) {
            mashing_timer.SetTimer(1, 30, 0);
          } else {
            mashing_timer.SetTimer(mashing_timer.ShowHours(), mashing_timer.ShowMinutes() + 5, mashing_timer.ShowSeconds());
          }

          if (rest_times[rest_index] <= 85) {
            rest_times[rest_index] += 5;
          } else {
            rest_times[rest_index] = 90;
          }

          if (rest_times[rest_index] >= 60) {
            rest_times_in_hours[rest_index] = rest_times[rest_index] / 60;
            rest_times_in_minutes[rest_index] = rest_times[rest_index] - 60;
          } else {
            rest_times_in_hours[rest_index] = 0;
            rest_times_in_minutes[rest_index] = rest_times[rest_index];
          }
        }
        break;
    }

    raw_serial_data = "";
  }

  //*****************************************************************************
  // Verarbeite Temperatursensoren          // Process temperature data
  //*****************************************************************************
  if (current_time - temp_sensor_time > sensor_time) {
    temp_sensor_time = current_time;    

    sensors.requestTemperatures();

    getTemperatureHgOne();
    getTemperatureHgTwo();
    getTemperatureNgOne();
    getTemperatureNgTwo();

    //***************************************************************************************
    // Temperaturen für Hauptguss und Nachguss          // Temperature for mashing water and sparging water
    //***************************************************************************************
    if (sensor_hg_one_ok) {
      current_hg_temperature = getTemperatureHgOne();
    } else if (sensor_hg_two_ok) {
      current_hg_temperature = getTemperatureHgTwo();
    } else {
      current_hg_temperature = -127.00;
    }

    if (sensor_ng_one_ok) {
      current_ng_temperature = getTemperatureNgOne();
    } else if (sensor_ng_two_ok) {
      current_ng_temperature = getTemperatureNgTwo();
    } else {
      current_ng_temperature = -127.00;
    }
  }

  //*****************************************************************************
  // Sende Daten an Android         // Send data to Android
  //*****************************************************************************
  if (current_time - temp_serial_time > serial_time) {
    temp_serial_time = current_time;

    switch (controller_state) {
      case 0:  
        if (lcd_enable) {
          lcd.setCursor(0, 0);
          lcd.print(version_number);

          lcd.setCursor(0, 1);
          lcd.print("                    ");

          lcd.setCursor(0, 2);
          lcd.print("  Warte auf Rezept  ");

          lcd.setCursor(0, 3);
          lcd.print("                    ");
        }

        send_data.concat('W');
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }

        send_data = "";
        break;
      case 1:
        //***********************************************************************
        // Einmaischen gestartet          // Mashing in started
        //***********************************************************************
        if (lcd_enable) {
          lcd.setCursor(0, 0);
          lcd.print("--- Einmaischen: ---");

          lcd.setCursor(0, 1);
          lcd.print("Ist:        ");          
          lcd.print(current_hg_temperature);
          lcd.print(" ");
          lcd.print((char)223);
          lcd.print("C");

          lcd.setCursor(0, 2);
          lcd.print("Soll:          ");
          lcd.print(target_mashing_temperature);
          lcd.print(" ");
          lcd.print((char)223);
          lcd.print("C");

          lcd.setCursor(0, 3);
          lcd.print("                    ");
        }

        send_data.concat('I');
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(delimiter);
        send_data.concat(target_mashing_temperature);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {          
          Serial2.println(send_data);
        }    

        send_data = "";
        break;
      case 2:
        //***********************************************************************
        // Einmaischen beendet          // Mashing in finished
        //************************************************************************
        if (lcd_enable) { 
          lcd.setCursor(0, 0);
          lcd.print("                    ");
          
          lcd.setCursor(0, 1);
          lcd.print("     Einmaischen    ");
          
          lcd.setCursor(0, 2);          
          lcd.print("       beendet      ");
          
          lcd.setCursor(0, 3);
          lcd.print("                    ");
        }

        send_data.concat('/');
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(delimiter);
        send_data.concat(target_mashing_temperature);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }    

        send_data = "";
        break;
      case 3:
        //***********************************************************************
        // Maischen gestartet         // Mashing started
        //***********************************************************************
        if (lcd_enable) {
          lcd.setCursor(0, 0);
          lcd.print("----- ");
          lcd.print(rest_index + 1);
          lcd.print(". Rast: -----");

          lcd.setCursor(0, 1);
          lcd.print("Ist:        ");          
          lcd.print(current_hg_temperature);
          lcd.print(" ");
          lcd.print((char)223);
          lcd.print("C");

          lcd.setCursor(0, 2);
          lcd.print("Soll:          ");          
          lcd.print(rest_temperatures[rest_index]);
          lcd.print(" ");
          lcd.print((char)223);
          lcd.print("C");

          lcd.setCursor(0, 3);
          lcd.print("Verbleibend: ");
          lcd.print(current_time_in_hours);
          lcd.print(":");
          lcd.print(current_time_in_minutes);
          lcd.print(":");
          lcd.print(current_time_in_seconds);
        }

        current_time_in_hours = mashing_timer.ShowHours();
        current_time_in_minutes = mashing_timer.ShowMinutes();
        current_time_in_seconds = mashing_timer.ShowSeconds();

        send_data.concat('M');
        send_data.concat(rest_index);
        send_data.concat(delimiter);
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(delimiter);
        send_data.concat(rest_temperatures[rest_index]);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_hours);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_minutes);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_seconds);
        send_data.concat(delimiter);
        send_data.concat(rest_times_in_hours[rest_index]);
        send_data.concat(delimiter);
        send_data.concat(rest_times_in_minutes[rest_index]);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }    

        send_data = "";
        break;
      case 4:
        //***********************************************************************
        // Maischen beendet         // Mashing finished
        //***********************************************************************
        if (lcd_enable) {
          lcd.setCursor(0, 0);
          lcd.print("                    ");
          
          lcd.setCursor(0, 1);
          lcd.print("      Maischen      ");
          
          lcd.setCursor(0, 2);
          lcd.print("      beendet       ");

          lcd.setCursor(0, 3);
          lcd.print("                    ");
        }

        send_data.concat('#');
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }    

        send_data = "";
        break;
      case 5:
        //***********************************************************************
        // Nachgüsse gestartet          // Sparging started
        //***********************************************************************
        if (lcd_enable) {
          remaining_sparge_amount = target_sparge_amount - current_sparge_amount;

          lcd.setCursor(0, 0);
          lcd.print("--- Nachguesse:  ---");

          lcd.setCursor(0, 1);
          lcd.print("Ist:        ");
          lcd.print(current_ng_temperature);
          lcd.print(" ");
          lcd.print((char)223);
          lcd.print("C");

          lcd.setCursor(0, 2);
          lcd.print("Soll:          ");
          lcd.print(target_ng_temperature);
          lcd.print(" ");
          lcd.print((char)223);
          lcd.print("C");

          lcd.setCursor(0, 3);
          lcd.print("Verbleibend: ");
          lcd.print(remaining_sparge_amount);
          lcd.print(" L");
        }

        send_data.concat('S');
        send_data.concat(current_sparge_amount);
        send_data.concat(delimiter);
        send_data.concat(target_sparge_amount);
        send_data.concat(delimiter);
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(target_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(delimiter);
        send_data.concat(vwh_notification);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }    

        send_data = "";

        vwh_notification = "false";
        break;
      case 6:
        //***********************************************************************
        // Nachgüsse beendet          // Sparging finished
        //***********************************************************************
        if (lcd_enable) {
          lcd.setCursor(0, 0);
          lcd.print("                    ");
          
          lcd.setCursor(0, 1);
          lcd.print("     Nachguesse     ");
          
          lcd.setCursor(0, 2);
          lcd.print("      beendet       ");

          lcd.setCursor(0, 3);
          lcd.print("                    ");
        }

        send_data.concat('%');
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }    

        send_data = "";
        break;
      case 7:
        //***********************************************************************
        // Nachgüsse gestartet UND Kochen gestartet         // Sparging started AND cooking started
        //***********************************************************************
        if (lcd_enable) {
          if (current_time - temp_lcd_time > lcd_time) {
            temp_lcd_time = current_time;

            if (!lcd_swap) {
              remaining_sparge_amount = target_sparge_amount - current_sparge_amount;

              lcd.setCursor(0, 0);
              lcd.print("--- Nachguesse:  ---");

              lcd.setCursor(0, 1);
              lcd.print("Ist:        ");
              lcd.print(current_ng_temperature);
              lcd.print(" ");
              lcd.print((char)223);
              lcd.print("C");

              lcd.setCursor(0, 2);
              lcd.print("Soll:          ");
              lcd.print(target_ng_temperature);
              lcd.print(" ");
              lcd.print((char)223);
              lcd.print("C");

              lcd.setCursor(0, 3);
              lcd.print("Verbleibend: ");
              lcd.print(remaining_sparge_amount);
              lcd.print(" L");

              lcd_swap = true;
            } else {
              lcd.setCursor(0, 0);
              lcd.print("----- Kochen: ------");

              lcd.setCursor(0, 1);
              lcd.print("Ist:        ");
              lcd.print(current_hg_temperature);
              lcd.print(" ");
              lcd.print((char)223);
              lcd.print("C");

              lcd.setCursor(0, 2);
              lcd.print("Soll:          ");
              lcd.print(target_cooking_temperature);
              lcd.print(" ");
              lcd.print((char)223);
              lcd.print("C");

              if (current_time_in_minutes == 0 && current_time_in_seconds == 0) {
                lcd.setCursor(0, 3);
                lcd.print("Verbleibend:   ");                
              } else {
                lcd.setCursor(0, 3);
                lcd.print("Verbleibend: ");                
              }

              lcd.print(current_time_in_hours);
              lcd.print(":");
              lcd.print(current_time_in_minutes);
              lcd.print(":");
              lcd.print(current_time_in_seconds);

              lcd_swap = false;
            }
          }
        }

        current_time_in_hours = cooking_timer.ShowHours();
        current_time_in_minutes = cooking_timer.ShowMinutes();
        current_time_in_seconds = cooking_timer.ShowSeconds();

        current_hop_time_in_hours = hop_timer.ShowHours();
        current_hop_time_in_minutes = hop_timer.ShowMinutes();
        current_hop_time_in_seconds = hop_timer.ShowSeconds();

        send_data.concat('N');
        send_data.concat(hop_notification);
        send_data.concat(delimiter);
        send_data.concat(hop_index);
        send_data.concat(delimiter);
        send_data.concat(current_sparge_amount);
        send_data.concat(delimiter);
        send_data.concat(target_sparge_amount);
        send_data.concat(delimiter);
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(target_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(delimiter);
        send_data.concat(target_cooking_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_hours);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_minutes);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_seconds);
        send_data.concat(delimiter);
        send_data.concat(target_cooking_time_in_hours);
        send_data.concat(delimiter);
        send_data.concat(target_cooking_time_in_minutes);
        send_data.concat(delimiter);
        send_data.concat(current_hop_time_in_hours);
        send_data.concat(delimiter);
        send_data.concat(current_hop_time_in_minutes);
        send_data.concat(delimiter);
        send_data.concat(current_hop_time_in_seconds);
        send_data.concat(delimiter);
        send_data.concat(vwh_notification);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }    

        send_data = "";

        hop_notification = "false";
        vwh_notification = "false";
        break;
      case 8:
        //***********************************************************************
        // Nachgüsse beendet UND Kochen gestartet         // Sparging finished AND cooking started
        //***********************************************************************
        if (lcd_enable) {
          lcd.setCursor(0, 0);
          lcd.print("----- Kochen: ------");

          lcd.setCursor(0, 1);
          lcd.print("Ist:        ");
          lcd.print(current_hg_temperature);
          lcd.print(" ");
          lcd.print((char)223);
          lcd.print("C");

          lcd.setCursor(0, 2);
          lcd.print("Soll:          ");
          lcd.print(target_cooking_temperature);
          lcd.print(" ");
          lcd.print((char)223);
          lcd.print("C");

          if (current_time_in_minutes == 0 && current_time_in_seconds == 0) {
            lcd.setCursor(0, 3);
            lcd.print("Verbleibend:   ");                
          } else {
            lcd.setCursor(0, 3);
            lcd.print("Verbleibend: ");                
          }

          lcd.print(current_time_in_hours);
          lcd.print(":");
          lcd.print(current_time_in_minutes);
          lcd.print(":");
          lcd.print(current_time_in_seconds);
        }

        current_time_in_hours = cooking_timer.ShowHours();
        current_time_in_minutes = cooking_timer.ShowMinutes();
        current_time_in_seconds = cooking_timer.ShowSeconds();

        current_hop_time_in_hours = hop_timer.ShowHours();
        current_hop_time_in_minutes = hop_timer.ShowMinutes();
        current_hop_time_in_seconds = hop_timer.ShowSeconds();

        send_data.concat('K');
        send_data.concat(hop_notification);
        send_data.concat(delimiter);
        send_data.concat(hop_index);
        send_data.concat(delimiter);
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(delimiter);
        send_data.concat(target_cooking_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_hours);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_minutes);
        send_data.concat(delimiter);
        send_data.concat(current_time_in_seconds);
        send_data.concat(delimiter);
        send_data.concat(target_cooking_time_in_hours);
        send_data.concat(delimiter);
        send_data.concat(target_cooking_time_in_minutes);
        send_data.concat(delimiter);
        send_data.concat(current_hop_time_in_hours);
        send_data.concat(delimiter);
        send_data.concat(current_hop_time_in_minutes);
        send_data.concat(delimiter);
        send_data.concat(current_hop_time_in_seconds);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }    

        send_data = "";

        hop_notification = "false";
        break;
      case 9:
        //***********************************************************************
        // Kochen beendet         // Cooking finished
        //***********************************************************************
        if (lcd_enable) {
          lcd.setCursor(0, 0);
          lcd.print("                    ");
          
          lcd.setCursor(0, 1);
          lcd.print("       Kochen       ");
          
          lcd.setCursor(0, 2);
          lcd.print("       beendet      ");

          lcd.setCursor(0, 3);
          lcd.print("                    ");
        }

        send_data.concat('&');
        send_data.concat(current_hg_temperature);
        send_data.concat(delimiter);
        send_data.concat(current_ng_temperature);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lg);
        send_data.concat(delimiter);
        send_data.concat(hysteresis_lb);
        send_data.concat(delimiter);
        send_data.concat(pump_lg_duration);
        send_data.concat(delimiter);
        send_data.concat(pump_lb_duration);
        send_data.concat(termination);

        if (bluetooth) {
          Serial1.println(send_data);
        } else {
          Serial2.println(send_data);
        }    

        send_data = "";
        break;
    }
  }

  //************************************************************************************************************************************
  // Warte auf Einmaischtemperatur                                    // Wait for mashing in temperature
  // Wenn Temperatur erreicht, regle weiter bis bestätigung von       // When the temperature is reached, the controller will continue
  // Android empfangen wurde                                          // to hold the temperature until the acknowledgement from Androis is received
  //************************************************************************************************************************************
  if (heater_hg_state_automatic) {
    if (controller_state == 1 || controller_state == 2) {
      if (sensor_hg_one_ok || sensor_hg_two_ok) {
        if (current_hg_temperature > target_mashing_temperature - hysteresis_lg && current_hg_temperature != 85.00 && millis() >= heater_hg_switching_time + 5000UL) {
          heater_hg_switching_time = millis();

          controller_state = 2;

          digitalWrite(heater_hg_out, heater_hg_out_OFF);
        } else if (current_hg_temperature <= target_mashing_temperature - hysteresis_lg && current_hg_temperature != 85.00 && millis() >= heater_hg_switching_time + 5000UL) {
          heater_hg_switching_time = millis();

          digitalWrite(heater_hg_out, heater_hg_out_ON);
        }
      } else {
        digitalWrite(heater_hg_out, heater_hg_out_OFF);
      }
    }
  } else if (heater_hg_state_manual) {
    digitalWrite(heater_hg_out, heater_hg_out_ON);
  } else {
    digitalWrite(heater_hg_out, heater_hg_out_OFF);
  }

  //***********************************************************
  // Starte Maischen          // Start mashing
  //***********************************************************
  if (heater_hg_state_automatic) {
    if (controller_state == 3) {
      if (rest_index < rests_amount) {
        if (sensor_hg_one_ok || sensor_hg_two_ok) {
          if (current_hg_temperature > rest_temperatures[rest_index] - hysteresis_lg && current_hg_temperature != 85.00 && millis() >= heater_hg_switching_time + 5000UL) {
            heater_hg_switching_time = millis();

            if (!timer_mashing_started) {
              mashing_timer.SetTimer(rest_times_in_hours[rest_index], rest_times_in_minutes[rest_index], 0);
              mashing_timer.StartTimer();

              timer_mashing_started = true;
            }

            digitalWrite(heater_hg_out, heater_hg_out_OFF);
          }

          if (current_hg_temperature <= rest_temperatures[rest_index] - hysteresis_lg && current_hg_temperature != 85.00 && millis() >= heater_hg_switching_time + 5000UL) {
            heater_hg_switching_time = millis();

            digitalWrite(heater_hg_out, heater_hg_out_ON);
          }
        } else {
          digitalWrite(heater_hg_out, heater_hg_out_OFF);
        }

        if (timer_mashing_started) {
          if (TimeCheck(mashing_timer, 0, 0, 0)) {
            mashing_timer.StopTimer();
            timer_mashing_started = false;

            rest_index++;
          }
        }
      } else {
        controller_state = 4;

        digitalWrite(heater_hg_out, heater_hg_out_OFF);
      }
    }
  } else if (heater_hg_state_manual) {
    digitalWrite(heater_hg_out, heater_hg_out_ON);
  } else {
    digitalWrite(heater_hg_out, heater_hg_out_OFF);
  }

  //***********************************************************
  // Starte Nachgüsse         // Start sparging
  //***********************************************************
  if (pump_lb_state_automatic) {
    if (controller_state == 5 || controller_state == 7) {      
      if (current_sparge_amount <= target_sparge_amount + water_adjustment) {
        if (ng_temperature_reached) {
          if (!level_switch_lb_full) {
            if (TimeCheck(pump_lb_timer, 0, 0, 0)) {
              pump_lb_timer.StopTimer();

              digitalWrite(pump_lb_out, pump_lb_out_ON);
            }
          } else {
            pump_lb_timer.SetTimer(0, 0, pump_lb_duration);
            pump_lb_timer.StartTimer();

            digitalWrite(pump_lb_out, pump_lb_out_OFF);
          }
        } else {
          digitalWrite(pump_lb_out, pump_lb_out_OFF);          
        }
      } else {
        digitalWrite(pump_lb_out, pump_lb_out_OFF);       

        if (controller_state == 5) {
          controller_state = 6;
        } else {
          controller_state = 8;
        }

        ng_enable = false;
      }

      if (!vwh_notification_sent && is_vwh.equals("true")) {        
          vwh_notification = "true";

          vwh_notification_sent = true;        
      }
    }
  } else if (pump_lb_state_manual) {
    digitalWrite(pump_lb_out, pump_lb_out_ON);    
  } else {
    digitalWrite(pump_lb_out, pump_lb_out_OFF);    
  }

  //***********************************************************
  // Starte Kochen          // Start cooking
  //***********************************************************
  if (heater_hg_state_automatic) {
    if (controller_state == 7 || controller_state == 8) {
      if (sensor_hg_one_ok || sensor_hg_two_ok) {
        digitalWrite(heater_hg_out, heater_hg_out_ON);
      } else {
        digitalWrite(heater_hg_out, heater_hg_out_OFF);
      }

      if (current_hg_temperature >= target_cooking_temperature) {
        if (!timer_cooking_started) {
          cooking_timer.SetTimer(target_cooking_time_in_hours, target_cooking_time_in_minutes, 2);
          cooking_timer.StartTimer();

          timer_cooking_started = true;
        }
      }

      if (timer_cooking_started) {
        if (TimeCheck(cooking_timer, 0, 0, 0)) {
          cooking_timer.StopTimer();

          timer_cooking_started = false;

          controller_state = 9;

          digitalWrite(heater_hg_out, heater_hg_out_OFF);
        }

        if (hop_index < hops_amount) {
          if (!timer_hop_started) {
            if (hop_index == 0) {
              if (target_cooking_time - hop_times[hop_index] >= 60) {
                hop_timer.SetTimer(target_cooking_time_in_hours - hop_times_in_hours[hop_index], target_cooking_time_in_minutes - hop_times_in_minutes[hop_index], 0);
              } else {
                hop_timer.SetTimer(0, target_cooking_time - hop_times[hop_index], 0);
              }
            } else {
              if (hop_times[hop_index - 1] - hop_times[hop_index] >= 60) {
                hop_timer.SetTimer(hop_times_in_hours[hop_index - 1] - hop_times_in_hours[hop_index], hop_times_in_minutes[hop_index - 1] - hop_times_in_minutes[hop_index], 0);
              } else {
                hop_timer.SetTimer(0, hop_times[hop_index - 1] - hop_times[hop_index], 0);
              }
            }

            hop_timer.StartTimer();

            timer_hop_started = true;
          }

          if (timer_hop_started) {
            if (TimeCheck(hop_timer, 0, 0, 0)) {
              hop_timer.StopTimer();

              timer_hop_started = false;

              hop_notification = "true";
              hop_index++;
            }
          }
        }
      }
    }
  } else if (heater_hg_state_manual) {
    digitalWrite(heater_hg_out, heater_hg_out_ON);
  } else {
    digitalWrite(heater_hg_out, heater_hg_out_OFF);
  }
}

//********************************************************************************************************
// Lese Temperatur von Sensor Hauptguss eins          // Read temperature from mashing water sensor one
//********************************************************************************************************
float getTemperatureHgOne() {
  float temp_hg_one = sensors.getTempC(sensor_hg_one);  

  if (temp_hg_one == -127.00) {
    sensor_hg_one_ok = false;
  } else {
    sensor_hg_one_ok = true;
  }

  return temp_hg_one;
}

//********************************************************************************************************
// Lese Temperatur von Sensor Hauptguss zwei          // Read temperature from mashing water sensor two
//********************************************************************************************************
float getTemperatureHgTwo() {  
  float temp_hg_two = sensors.getTempC(sensor_hg_two);

  if (temp_hg_two == -127.00) {
    sensor_hg_two_ok = false;
  } else {
    sensor_hg_two_ok = true;
  }

  return temp_hg_two;
}

//********************************************************************************************************
// Lese Temperatur von Sensor Nachguss eins         // Read temperature from sparging water sensor one
//********************************************************************************************************
float getTemperatureNgOne() {
  float temp_ng_one = sensors.getTempC(sensor_ng_one);

  if (temp_ng_one == -127.00) {
    sensor_ng_one_ok = false;
  } else {
    sensor_ng_one_ok = true;
  }

  return temp_ng_one;
}

//********************************************************************************************************
// Lese Temperatur von Sensor Nachguss zwei         // Read temperature from sparging water sensor two
//********************************************************************************************************
float getTemperatureNgTwo() {
  float temp_ng_two = sensors.getTempC(sensor_ng_two);

  if (temp_ng_two == -127.00) {
    sensor_ng_two_ok = false;
  } else {
    sensor_ng_two_ok = true;
  }

  return temp_ng_two;
}

//********************************************************************************************************
// Splitte Bluetooth String empfangen von Android         // Split bluetooth strings from Android
//********************************************************************************************************
String splitString(const String & data, char separator, int index) {
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

//********************************************************************************************************
// Überprüfe Zeit des Timers              // Check time of the timers
//********************************************************************************************************
boolean TimeCheck(CountUpDownTimer timer, unsigned int hours, unsigned int minutes, unsigned int seconds) {
  return (hours == timer.ShowHours() && minutes == timer.ShowMinutes() && seconds == timer.ShowSeconds());
}

//********************************************************************************************************
// Interrupt Service Routine - Erhöhe die Impulse der         // Interrupt service routine - Increase the
// Variable für den Wasserzähler                              // impulse variable for the water counter
//********************************************************************************************************
void increasePulses() {
  if (controller_state == 5 || controller_state == 7) {
    pulses++;
  }
}


//********************************************************************************************************
// Spielt eine kurze Melodie                                   // Plays a short melody
//********************************************************************************************************
void beep() {  
  if (current_time - temp_speaker_time >= note_duration + note_pause) {
  temp_speaker_time = current_time;
  
    if (melody_index < sizeof(melody) / 2) {      
      tone(speaker_out, melody[melody_index], note_duration);
      melody_index++;      
    } else {      
      melody_play_state[melody_play_state_index] = true; 

      melody_index = 0;
      melody_play_state_index++;
    }
  } 
}

//********************************************************************************************************
// Zeigt Debugging-Information                                   // Shows debugging information
//********************************************************************************************************
void printDebugging() {
  if (current_time - temp_debugging_time > debugging_time) {
    temp_debugging_time = current_time;    
  
    Serial.println("************************************************");
    Serial.println("*             " + String(version_number) + "             *");   
    Serial.println("************************************************");
  
    Serial.print("Brauanlagen Status: ");
  
    switch (controller_state) {
      case 0:
        Serial.println("Warte auf Rezept");
        break;
      case 1:
        Serial.println("Einmaischen gestartet");
        break;
      case 2:
        Serial.println("Einmaischen beendet");
        break;
      case 3:
        Serial.println("Maischen gestartet");
        break;
      case 4:
        Serial.println("Maischen beendet");
        break;
      case 5:
        Serial.println("Nachguesse gestartet");
        break;
      case 6:
        Serial.println("Nachguesse beendet");
        break;
      case 7:
        Serial.println("Nachguesse gestartet UND Kochen gestartet");
        break;
      case 8:
        Serial.println("Nachguesse beendet UND Kochen gestaret");
        break;
      case 9:
        Serial.println("Kochen beendet");
        break;
    }  

    Serial.println();   
    Serial.println("-     Eingaenge     -");    

    Serial.println("Heizung Hauptguss:\t\t" + String(heater_hg_state_manual ? "Manuell" : (heater_hg_state_automatic ? "Automatik" : "Aus")));
    Serial.println("Heizung Nachguss:\t\t" + String(heater_ng_state_manual ? "Manuell" : (heater_ng_state_automatic ? "Automatik" : "Aus")));
    Serial.println("Pumpe Laeutergrant:\t\t" + String(pump_lg_state_manual ? "Manuell" : (pump_lg_state_automatic ? "Automatik" : "Aus")));
    Serial.println("Pumpe Laeuterbottich:\t\t" + String(pump_lb_state_manual ? "Manuell" : (pump_lb_state_automatic ? "Automatik" : "Aus")));
    Serial.println("Ruehrwerk:\t\t\t" + String(stirrer_state_manual ? "Manuell" : (stirrer_state_automatic ? "Automatik" : "Aus")));
    Serial.println("Niveauschalter Laeuterbottich:\t" + String(level_switch_lg_full ? "Voll" : "Leer"));
    Serial.println("Niveauschalter Laeuterbottich:\t" + String(level_switch_lb_full ? "Voll" : "Leer"));   

    Serial.println();   
    Serial.println("-     Ausgaenge     -");  

    Serial.println("Heizung Hauptguss:\t\t" + String(digitalRead(heater_hg_out) == heater_hg_out_ON ? "Ein" : "Aus"));
    Serial.println("Heizung Nachgussguss:\t\t" + String(digitalRead(heater_ng_out) == heater_ng_out_ON ? "Ein" : "Aus"));
    Serial.println("Pumpe Laeutergrant:\t\t" + String(digitalRead(pump_lg_out) == pump_lg_out_ON ? "Ein" : "Aus"));
    Serial.println("Pumpe Laeuterbottich:\t\t" + String(digitalRead(pump_lb_out) == pump_lb_out_ON ? "Ein" : "Aus"));
    Serial.println("Ruehrwerk:\t\t\t" + String(digitalRead(stirrer_out) == stirrer_out_ON ? "Ein" : "Aus"));

    Serial.println();
    Serial.println("-     Etc     -"); 
    Serial.println("Verbindung:\t\t\t" + String(bluetooth ? "Bluetooth" : "WiFi"));
    Serial.println("Hauptguss Temperatur:\t\t" + String(current_hg_temperature) + " C");
    Serial.println("Nachguss Temperatur:\t\t" + String(current_ng_temperature) + " C");
    Serial.println("Nachguss Temperatur erreicht:\t" + String(ng_temperature_reached ? "Ja" : "Nein"));
    Serial.println("Anzahl an Impulsen:\t\t" + String(pulses));    
      
    Serial.println();
    Serial.println("-     Rezept     -"); 

    if (rests_amount > 0) {
      Serial.println("Einmaischtemperatur:\t\t" + String(target_mashing_temperature) + " C");      
      for (int i = 0; i < rests_amount; i++) {
        Serial.println("Rast " + String(i + 1) + ":\t\t\t\t" + String(rest_temperatures[i]) + " C\t\t" + String(rest_times[i]) + " Min");
      }
      
      Serial.println();
      Serial.println("Nachguss:\t\t\t" + String(current_sparge_amount) + " L" + " / " + String(target_sparge_amount) + " L");
      Serial.println();
      
      Serial.println("Kochzeit:\t\t\t" + String(target_cooking_time));
      for (int i = 0; i < hops_amount; i++) {
        Serial.println("Hopfengabe " + String(i + 1) + ":\t\t\t" + String(hop_times[i]) + " Min");
      }
      Serial.println();
    } else {
      Serial.println("Kein Rezept\n");
    }
  }  
}